package archive;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;

public class PurchasesArchiveTest {

    Item first;
    Item second;
    Item third;
    PurchasesArchive archive;
    ArrayList<Order> orders;

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(out));
        first = new StandardItem(1, "First", 8787.89F, "Category1", 420);
        second = new StandardItem(2, "Second", 0.89F, "Category2", 1);
        third = new StandardItem(3, "Third", 74.054F, "Category3", 89);
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void printItemPurchaseStatisticsTest(){
        ItemPurchaseArchiveEntry purchasedFirst = new ItemPurchaseArchiveEntry(first);
        ItemPurchaseArchiveEntry purchasedSecond = new ItemPurchaseArchiveEntry(second);
        ItemPurchaseArchiveEntry purchasedThird = new ItemPurchaseArchiveEntry(third);

        HashMap<Integer, ItemPurchaseArchiveEntry> hashMap = new HashMap<>();
        hashMap.put(1, purchasedFirst);
        hashMap.put(2, purchasedSecond);
        hashMap.put(3, purchasedThird);

        archive = new PurchasesArchive(hashMap, null);

        String phrase = "ITEM PURCHASE STATISTICS:";

        archive.printItemPurchaseStatistics();

        Assertions.assertEquals(phrase+System.lineSeparator()+purchasedFirst.toString()+
                System.lineSeparator()+purchasedSecond.toString()
                +System.lineSeparator()+purchasedThird.toString()+ System.lineSeparator(), out.toString());

    }

    @Test
    public void getHowManyTimesHasBeenItemSoldTest(){
        orders = new ArrayList<>();
        HashMap<Integer, ItemPurchaseArchiveEntry> hashMap = new HashMap<>();

        ItemPurchaseArchiveEntry purchasedFirst = Mockito.mock(ItemPurchaseArchiveEntry.class);
        ItemPurchaseArchiveEntry purchasedSecond = Mockito.mock(ItemPurchaseArchiveEntry.class);
        ItemPurchaseArchiveEntry purchasedThird = Mockito.mock(ItemPurchaseArchiveEntry.class);

        hashMap.put(1, purchasedFirst);
        hashMap.put(2, purchasedSecond);
        hashMap.put(3, purchasedThird);

        archive = new PurchasesArchive(hashMap, orders);

        Mockito.doReturn(87).when(purchasedFirst).getCountHowManyTimesHasBeenSold();
        Mockito.doReturn(6).when(purchasedSecond).getCountHowManyTimesHasBeenSold();
        Mockito.doReturn(48).when(purchasedThird).getCountHowManyTimesHasBeenSold();

        int firstResult = archive.getHowManyTimesHasBeenItemSold(first);
        int secondResult = archive.getHowManyTimesHasBeenItemSold(second);
        int thirdResult = archive.getHowManyTimesHasBeenItemSold(third);

        Assertions.assertEquals(87, firstResult);
        Assertions.assertEquals(6, secondResult);
        Assertions.assertEquals(48, thirdResult);



    }

    @Test
    public void putOrderToPurchasesArchiveTest(){
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(first);
        cart.addItem(second);
        cart.addItem(third);

        Order order = new Order(cart, "Lenka", "Opletalova 8", 1);

        archive = new PurchasesArchive();
        int firstSold = archive.getHowManyTimesHasBeenItemSold(first);
        int secondSold = archive.getHowManyTimesHasBeenItemSold(second);
        int thirdSold = archive.getHowManyTimesHasBeenItemSold(third);

        archive.putOrderToPurchasesArchive(order);

        int firstSoldMinusOne = archive.getHowManyTimesHasBeenItemSold(first) - 1;
        int secondSoldMinusOne = archive.getHowManyTimesHasBeenItemSold(second) - 1;
        int thirdSoldMinusOne = archive.getHowManyTimesHasBeenItemSold(third) - 1;

        Assertions.assertEquals(firstSold, firstSoldMinusOne);
        Assertions.assertEquals(secondSold, secondSoldMinusOne);
        Assertions.assertEquals(thirdSold, thirdSoldMinusOne);
    }
}
