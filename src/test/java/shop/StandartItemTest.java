package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;

public class StandartItemTest {
    int id = 1;
    String name = "Supreme Box Logo";
    float price = (float) 599.99;
    String category = "Tees";
    int loyaltyPoints = 50;


    @Test
    public void constructorTest() {
        StandardItem result = new StandardItem(id, name, price, category, loyaltyPoints);

        Assertions.assertEquals(id, result.getID());
        Assertions.assertEquals(name, result.getName());
        Assertions.assertEquals(price, result.getPrice());
        Assertions.assertEquals(category, result.getCategory());
        Assertions.assertEquals(loyaltyPoints, result.getLoyaltyPoints());
    }

    @Test
    public void copyTest(){
        StandardItem expected = new StandardItem(id, name, price, category, loyaltyPoints);

        StandardItem result = expected.copy();

        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> data() {
        Stream<Arguments> data = Stream.of(
                Arguments.of(new StandardItem(10, "Name", 200.59F, "Cat", 50),
                        new StandardItem(10, "Name", 200.59F, "Cat", 50), true),
                Arguments.of(new StandardItem(1, "Name", 100, "Cat", 1),
                        new StandardItem(1, "Name", 100.1F, "Cat", 1), false),
                Arguments.of(new StandardItem(5, "Name", 20.54F, "Cat", 1),
                        new StandardItem(6, "Name", 20.54F, "Cat", 1), false)
        );
        return data;
    }

    @ParameterizedTest(name = "First object {0}, second object {1}, result {2}")
    @MethodSource("data")
    public void returnTrueIfObjectIsItem_or_returnFalseIfNot(Object first, Object second, boolean expected) {
        boolean result = first.equals(second);

        Assertions.assertEquals(expected, result);

    }
}
