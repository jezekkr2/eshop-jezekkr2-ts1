package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;

import java.util.ArrayList;

public class OrderTest {

    @Test
    public void constructorTest_withState() {
        Item item = new StandardItem(1, "Name", 54.1F, "Cat", 10);
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(item);
        ShoppingCart cart = new ShoppingCart(items);
        String customerName = "Lenka";
        String customerAdress = "Lebedova 8";
        int state = 5;

        Order result = new Order(cart, customerName, customerAdress, state);

        Assertions.assertEquals(cart.getCartItems(), result.getItems());
        Assertions.assertEquals(customerName, result.getCustomerName());
        Assertions.assertEquals(customerAdress, result.getCustomerAddress());
        Assertions.assertEquals(state, result.getState());
    }

    @Test
    public void constructorTest_withoutState() {
        Item item = new StandardItem(1, "Name", 54.1F, "Cat", 10);
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(item);
        ShoppingCart cart = new ShoppingCart(items);
        String customerName = "Lenka";
        String customerAdress = "Lebedova 8";
        int state = 0;

        Order result = new Order(cart, customerName, customerAdress);

        Assertions.assertEquals(cart.getCartItems(), result.getItems());
        Assertions.assertEquals(customerName, result.getCustomerName());
        Assertions.assertEquals(customerAdress, result.getCustomerAddress());
        Assertions.assertEquals(state, result.getState());

    }
}
