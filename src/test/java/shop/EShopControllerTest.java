package shop;

import archive.PurchasesArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import storage.NoItemInStorage;
import storage.Storage;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static shop.EShopController.*;

public class EShopControllerTest {

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp(){
        EShopController.startEShop();
    }

    @Test
    public void shoppingCartProcessTest() throws NoItemInStorage {
        Item item = new StandardItem(1, "Name", 90.25F, "Category", 1);
        ShoppingCart cart = EShopController.newCart();
        Storage storage = new Storage();
        PurchasesArchive archive = new PurchasesArchive();
        cart.addItem(item);

        // Test if cart addItem works properly
        Assertions.assertTrue(cart.getCartItems().contains(item));

        // Test if buying an item that is not in storage throws NoItemInStorage exception
        Assertions.assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(cart, "Libuse Novakova", "Kosmonautu 25, Praha 8", storage, archive));

        // Lets add another item and remove it to test if removing works
        Item item2 = new StandardItem(2, "Dancing Panda v.2", 5000, "GADGETS", 5);
        cart.addItem(item2);
        cart.removeItem(2);
        Assertions.assertFalse(cart.getCartItems().contains(item2));
        // Needed to fix the removeItem for cycle i <= 0 -> i >= 0

        // Now insert item into storage and try to buy the cart
        storage.insertItems(item, 10);
        EShopController.purchaseShoppingCart(cart, "Libus Novakova", "Kosmonautu 25", storage, archive);
        Assertions.assertEquals(storage.getItemCount(item2), archive.getHowManyTimesHasBeenItemSold(item2));

        // Now try to buy an empty cart
        System.setOut(new PrintStream(out));
        ShoppingCart emptyCart = EShopController.newCart();
        String expectedMessage = "Error: shopping cart is empty"+System.lineSeparator();
        Assertions.assertDoesNotThrow(() ->
                EShopController.purchaseShoppingCart(emptyCart, "Libus Novakova", "Astronautu 509", storage, archive)
        );
        Assertions.assertEquals(expectedMessage, out.toString());

    }

}
