package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;

public class ItemStockTest {
    Item item;
    ItemStock itemStock;

    @BeforeEach
    public void setUpItemStock(){
        item = new StandardItem(1, "Name", 54.1F, "Cat", 10);
        itemStock = new ItemStock(item);
    }

    @Test
    public void constructorTest() {
        Assertions.assertEquals(item, itemStock.getItem());
        Assertions.assertEquals(0,  itemStock.getCount());
    }

    @ParameterizedTest(name = "First increase by {0}, then decrease by {1}, then increase again by {2}. Result should equal {3}")
    @CsvSource({"2,1,3,4", "10,7,5,8", "0,0,0,0", "1,5,2,-2"})
    public void increaseThenDecreaseThenIncreaseTest(int first, int second, int third, int result) {
        itemStock.IncreaseItemCount(first);
        itemStock.decreaseItemCount(second);
        itemStock.IncreaseItemCount(third);

        Assertions.assertEquals(result, itemStock.getCount());

    }
}
